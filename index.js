const express = require('express');
const mongoose = require('mongoose');
// Allows us to control app's Cross Origin Resource Sharing settings
const cors = require('cors');
// Routes
const userRoutes = require('./routes/user')
const courseRoutes = require('./routes/course')


/*server setup*/
const app = express();
const port = 4000;
// Allows all resources/origin to access our backend application
// Enable all CORS requests
app.use(cors())

app.use(express.json());
app.use(express.urlencoded({
	extended: true
}))
// Defines the '/api/users' string to be included for all rutes defined in the 'user' route file

app.use('/api/users', userRoutes)
app.use('/api/courses', courseRoutes)


/*db connection*/
mongoose.connect("mongodb+srv://yaniiix21:clark123@wdc028-course-booking.y3iug.mongodb.net/batch144_booking_system?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We're connected to the cloud database"))


app.listen(port, () => console.log(`Now listening to port ${port}`))