const express = require("express")
const router = express.Router();
const courseController = require("../controllers/course")
const auth = require("../auth")

// Route for creating a course
router.post("/", (req,res) => {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
})

// Route for creating a course with auth


	/*courseController.addNewCourse(req.body)
.then(result => res.send(result))
*/


/*router.post('/create', auth.verify, (req,res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization)
	}.isAdmin

	courseController.addNewCourse(data).then(result => res.send(result))
})
*/
// all this result is from the controllers

router.post("/create", (req,res) => {
	courseController.addNewCourse(req.body).then(result => res.send(result))
})

// Retrive all courses
router.get("/all", (req,res) => {
	courseController.getAllCourses().then(result => res.send(result))
})
// Retrieve all active course
router.get("/", (req,res) => {
	courseController.getAllActive().then(result => res.send(result))
})
// Retrieve a specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);
	courseController.getCourse(req.params).then(result => res.send(result));
})

// update a course
router.put("/:courseId", auth.verify,(req,res) => {
	courseController.updateCourse(req.params, req.body).then(result => res.send(result))
})

// archiving a course
router.put("/:courseId/archive", auth.verify,(req,res) => {
	courseController.archiveCourse(req.params).then(result => res.send(result))
})

module.exports = router