const express = require('express');
const router = express.Router();
const auth = require("../auth")

const userController = require('../controllers/user');

// Route for checking ig the user's email already exists in the databse
router.post('/checkEmail', (req,res) =>{
	userController.checkEmailExists(req.body).then(result => res.send(result))
});


// Routes for User Registration
router.post('/register', (req,res) => {
	userController.registerUser(req.body).then(result => res.send(result))
})

// routes for aunthenticating a user
router.post('/login', (req,res) => {
	userController.loginUser(req.body).then(result => res.send(result))
})

// routes for user details
router.get('/details', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization)

/*	userController.userDetails(req.body).then(result => res.send(result))
})*/
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController))

})
// route to entroll user to a course
router.post("/enroll", (req,res) => {
	let data = {
		userId : req.body.userId,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))

})
// enroll with auth
router.post("/enrollements", auth.verify, (req,res) => {
	let data = {
		userId : req.body.userId,
		courseId: req.body.courseId
	}

	userController.enrollment(data).then(resultFromController => res.send(resultFromController))

})



module.exports = router;