const Course = require("../models/Course");
const auth = require("../auth")
const User = require("../models/User")


module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	// Saves the created object to the database
	return newCourse.save().then((course,error) => {
		// Course creation failed
		if(error){
			return false
		}
		// Course creation successfull
		else{
			return true;
		}
	})
}

/*
module.exports.addNewCourse = (data) => {

	if(data.isAdmin){

	let newCourse = new Course({
		name: data.course.name,
		description: data.course.description,
		price: data.course.price
	})

	// Saves the created object to the database
	return newCourse.save().then((course,error) => {
		// Course creation failed
		if(error){
			return false
		}
		// Course creation successfull
		else{
			return true;
		}
	})
}
}else{
	return false
}

*/










/*
LOGIC

1. Check if the authorization exist in out database, if not return false
2. Check if the user isAdmin if not return "User cannot create a Course"
2. Check if the course exist in out database, if exist return "course is already exist"
3.if course did not exist create the course and return "Course is added"
*/

module.exports.addNewCourse = (reqBody) => { 

	return  User.find({isAdmin: true}).then(result => {
		if(result == false){
			return false
		}else{
			let newCourse = new Course({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			})

			return newCourse.save().then((course,error) => {
				// Course creation failed
				if(error){
					return false
				}
				// Course creation successfull
				else{
					return true
				}
			})
		}
	})
}

// Retrieve all course
module.exports.getAllCourses = () =>{
	return Course.find({}).then(result => {
		return result
	})
}
// retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
}
// Retieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}
// Update a course
module.exports.updateCourse = (reqParams, reqBody) =>{
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course,error) =>{
		if (error) {
			return false
		}else{
			return true
		}
	})
}

// Archive a course
module.exports.archiveCourse = (reqParams) =>{
	let archivedCourse = {
		isActive: false
	}
	return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course,error) =>{
		if (error) {
			return false
		}else{
			return true
		}
	})
}
